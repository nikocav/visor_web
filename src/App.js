import './App.scss';
import Tabs from "./components/tabs/tabs";
import {StateProvider} from "./hooks/stateProvider/stateProvider";
import reducer from "./hooks/stateProvider/reducer";
import state from "./hooks/stateProvider/state";
import Graphics from "./components/graphics";

function App() {
  return (
    <div className="content">
        <h3>Visor causas de muertes EE.UU</h3>
        <StateProvider initialState={state} reducer={reducer}>
            <div className="content_footer">
              <Tabs />
              <Graphics/>
            </div>
        </StateProvider>
    </div>
  );
}

export default App;
