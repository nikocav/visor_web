import React, {useEffect, useState} from 'react';
import Chart from "react-apexcharts";
import useGlobalState from "../../hooks/stateProvider/stateProvider";
import {getStatsByCause} from "../../service/data";
import './style.scss';

const Index = () => {
    const [{selectedCause}] = useGlobalState();
    const [stats, setStats] = useState();
    const [year, setYear] = useState();

    const [graphCategories, setGraphCategories] = useState([]);
    const [graphData, setGraphData] = useState([]);

    useEffect(() => {
        if (selectedCause) {
            (async () => {
                const s = await getStatsByCause(selectedCause);
                setStats(s);
            })();
        }
    }, [selectedCause]);

    useEffect(() => {
        if (stats) {
            const y = Object.keys(stats)[0]
            setYear(y)
            setGraphCategories(Object.keys(stats[y]))
            setGraphData(stats[y])
        }
    }, [stats])

    useEffect(() => {
        if (year) {
            setGraphData(stats[year])
        }
    }, [year])

    return (
        <div className="graphics">
            <h3>{selectedCause && selectedCause.name}</h3>
            {stats &&
                <select onChange={(e) => setYear(e.target.value)}>
                    {Object.keys(stats).map(item => <option key={item} value={item}>{item}</option>)}
                </select>
            }
            {year &&
                <div className="graph">
                    <div className="chart">
                        <Chart
                            options={{
                                options: {
                                    chart: {
                                        id: "basic-bar"
                                    },
                                    xaxis: {
                                        categories: graphCategories
                                    }
                                }
                            }}
                            series={[
                                {
                                    name: "Cases",
                                    data: graphData
                                }
                            ]}
                            type="bar"
                        />
                    </div>
                    <table>
                        <thead>
                            <tr>
                                {graphCategories.map(item => <td key={item}>{Number(item) + 1}</td>)}
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                {graphData.map(month =>
                                    <td key={month}>{month}</td>
                                )}
                            </tr>

                        </tbody>
                    </table>
                </div>
            }
        </div>
    );
};

export default Index;