import React from 'react';
import {ReactComponent as HeartFilledIcon} from "../../assets/icons/heartFilled.svg";
import useGlobalState from "../../hooks/stateProvider/stateProvider";
import {ADD_OR_REMOVE_FAVORITE, SELECT_CAUSE} from "../../hooks/stateProvider/actionType";

const Favorites = () => {
    const [{favorites}, dispatch] = useGlobalState();

    const addOrRemoveToFavorites = (item) => {
        dispatch({type: ADD_OR_REMOVE_FAVORITE, payload: item});
    }

    const handlerClickItem = async (item) => {
        dispatch({type: SELECT_CAUSE, payload: item})
    }

    return (
        <ul>
            {favorites.length > 0 && favorites.map(item =>
                <li onClick={() => handlerClickItem(item)} key={item.id} className="cause_item">
                    <span className="cause_item_name">{item.name}</span>
                    <HeartFilledIcon onClick={() => addOrRemoveToFavorites(item)} className="heart"/>
                </li>
            )}
        </ul>
    );
};

export default Favorites;