import React, {useState} from 'react';
import Causes from "./causes";
import './style.scss'
import Favorites from "./favorites";

const tabsItems = Object.freeze({
    enfermedades: {
        key: 'enfermedades',
        label: 'Enfermedades'
    },
    favoritos:{
        key: 'favoritos',
        label:'Favoritos'
    }
})

const Tabs = () => {
    const [activeTab, setActiveTab] = useState(tabsItems.enfermedades.key);
    return (
        <div className="tabs">
            <ul className="tab">
                {Object.keys(tabsItems).map(key =>
                    <li key={key} className={activeTab === key ? 'selected' : ''} onClick={() => setActiveTab(key)}>{tabsItems[key].label}</li>
                )}
            </ul>
            {(activeTab === tabsItems.enfermedades.key) && (
                <Causes />
            )}
            {(activeTab === tabsItems.favoritos.key) && (
                <Favorites />
            )}
        </div>
    );
};

export default Tabs;