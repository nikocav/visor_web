import React, {useEffect, useState} from 'react';
import {getCauses} from "../../service/data";
import useGlobalState from "../../hooks/stateProvider/stateProvider";
import {ADD_OR_REMOVE_FAVORITE, SELECT_CAUSE, SET_CAUSES} from "../../hooks/stateProvider/actionType";
import { ReactComponent as HeartFilledIcon } from '../../assets/icons/heartFilled.svg';
import { ReactComponent as HeartIcon } from '../../assets/icons/heart.svg';

const Causes = () => {

    const [{causes, favorites}, dispatch] = useGlobalState();
    const [causesFilter, setCausesFilter ] = useState(causes);

    useEffect(() => {
        if (causes.length === 0) {
            getCauses().then(items => {
                dispatch({type: SET_CAUSES, payload: items});
                setCausesFilter(items);
            })
        }
    },[])

    const onFilter = (e) => {
      const value = e.target.value;
      if (value.length > 2) {
          const r = causes.filter(item => item.name.toLowerCase().includes(value));
          setCausesFilter(r);
      } else {
          setCausesFilter(causes);
      }
    }

    const addOrRemoveToFavorites = (item) => {
        dispatch({type: ADD_OR_REMOVE_FAVORITE, payload: item});
    }

    const isFavorite = (item) => {
      return favorites.find(fav => fav.id === item.id);
    }

    const handlerClickItem = async (item) => {
        dispatch({type: SELECT_CAUSE, payload: item})
    }

    return (
        <div className="causes_list">
            <input onChange={onFilter}/>
            <ul>
                {causesFilter.length > 0 && causesFilter.map(item =>
                    <li onClick={() => handlerClickItem(item)} key={item.id} className="cause_item">
                        <span className="cause_item_name">{item.name}</span>
                        {isFavorite(item) ?
                            <HeartFilledIcon onClick={() => addOrRemoveToFavorites(item)} className={`heart`}/> :
                            <HeartIcon onClick={() => addOrRemoveToFavorites(item)} className={`heart`}/>
                        }
                    </li>
                )}
            </ul>
        </div>
    );
};

export default Causes;