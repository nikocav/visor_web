const axios = require('axios');
const {config} = require('../config');

export const getCauses = async () => {
    const r = await axios.get(`${config.api_url}/causes`);

    if (r.status === 200 && r.data.success) {
        return r.data.payload
    }

    return [];
}

export const getStatsByCause = async (cause) => {
    const r = await axios.get(`${config.api_url}/list/${cause.id}`);

    if (r.status === 200 && r.data.success) {
        return r.data.payload
    }

    return [];
}