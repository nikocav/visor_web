import {ADD_OR_REMOVE_FAVORITE, SELECT_CAUSE, SET_CAUSES} from './actionType';

const reducer = (state = {}, action) => {
    switch (action.type) {
        case SET_CAUSES:
            return {...state, causes: action.payload}
        case ADD_OR_REMOVE_FAVORITE:
            const exist = state.favorites.find(fav => fav.id === action.payload.id);
            if (!exist) {
                return {...state, favorites: [...state.favorites, action.payload]}
            } else {
                const favs = state.favorites.filter(fav => fav.id !== action.payload.id);
                return {...state, favorites: favs}
            }
        case SELECT_CAUSE:
            return {...state, selectedCause: action.payload}
        default:
            return state;
    }
}

export default reducer;